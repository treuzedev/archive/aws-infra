#####
variable "main_vpc_subnets" {

  description = <<EOT
    A map containing maps configuring the main vpc subnets
    Each key is the name of the subnet.
    Each map should have three keys, namely, az, cidr and rt_name.
    Note that the rt_name must match one defined in the var main_vpc_rts.
    Defaults to:
    {
      "cloud9" : {
        "az" : "eu-west-1a",
        "cidr" : "10.0.0.0/28",
        "rt_name" : "public"
      }
    }
    EOT

  default = {
    "cloud9" : {
      "az" : "eu-west-1a",
      "cidr" : "10.0.0.0/28",
      "rt_name" : "public"
    }
  }

}

#####
variable "main_vpc_rts" {

  description = <<EOT
    Main vpc route tables. See default for more info.
    A map containing maps configuring each route table.
    The key is the name of the route table.
    Defaults to:
    {
      "public" : {
        "routes" : [
          {
            "cidr" : "0.0.0.0/0",
            "type" : "igw"
          }
        ]
      }
    }
    EOT

  default = {
    "public" : {
      "routes" : [
        {
          "cidr" : "0.0.0.0/0",
          "type" : "igw"
        }
      ]
    }
  }

}

#####
variable "main_vpc_sgs" {

  description = <<EOT
    A map containing maps configuring a security group.
    Defaults to:
    {
      "cloud9" : {
        "name" : "treuze-cloud9-public",
        "description" : "Allow access from home computer for testing purposes",
        "ingress" : [
          {
            "from_port" : 5000,
            "to_port" : 5001,
            cidr_blocks = [
              "85.247.16.123/32"
            ],
            "protocol" : "tcp",
            "description" : "Incoming from home, 5000 and 5001"
          },
          {
            "from_port" : 3000,
            "to_port" : 3000,
            cidr_blocks = [
              "85.247.16.123/32"
            ],
            "protocol" : "tcp",
            "description" : "Incoming from home, 3000"
          }
        ]
        "egress" : [
          {
            "from_port" : 0,
            "to_port" : 0,
            cidr_blocks = [
              "0.0.0.0/0"
            ],
            "protocol" : -1,
            "description" : "Allow all ipv4 traffic out"
          }
        ]
      }
    }
    EOT

  default = {
    "cloud9" : {
      "name" : "treuze-cloud9-public",
      "description" : "Allow access from home computer for testing purposes",
      "ingress" : [
        {
          "from_port" : 5000,
          "to_port" : 5001,
          cidr_blocks = [
            "85.247.16.123/32"
          ],
          "protocol" : "tcp",
          "description" : "Incoming from home, 5000 and 5001"
        },
        {
          "from_port" : 3000,
          "to_port" : 3000,
          cidr_blocks = [
            "85.247.16.123/32"
          ],
          "protocol" : "tcp",
          "description" : "Incoming from home, 3000"
        }
      ]
      "egress" : [
        {
          "from_port" : 0,
          "to_port" : 0,
          cidr_blocks = [
            "0.0.0.0/0"
          ],
          "protocol" : -1,
          "description" : "Allow all ipv4 traffic out"
        }
      ]
    }
  }

}
