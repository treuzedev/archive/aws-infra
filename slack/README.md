# Slack Integrations

## Incoming Webhooks

Info [here](https://api.slack.com/messaging/webhooks)

Channels configured:
* #test
* #aws-slack-integration

Secret ARN: arn:aws:secretsmanager:eu-west-1:189942472391:secret:TreuzeSlackIntegration-VNiMI8
