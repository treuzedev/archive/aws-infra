#####
resource "aws_security_group" "main" {

    for_each = var.sgs

    name = each.value.name
    description = each.value.description
    vpc_id = aws_vpc.main.id

    dynamic "ingress" {
        for_each = each.value.ingress
        content {
            from_port = ingress.value.from_port
            to_port = ingress.value.to_port
            cidr_blocks = ingress.value.cidr_blocks
            protocol = ingress.value.protocol
            description = ingress.value.description
        }
    }

    dynamic "egress" {
        for_each = each.value.egress
        content {
            from_port = egress.value.from_port
            to_port = egress.value.to_port
            cidr_blocks = egress.value.cidr_blocks
            protocol = egress.value.protocol
            description = egress.value.description
        }
    }

    tags = var.common_tags

}