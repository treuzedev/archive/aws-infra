#####
resource "aws_subnet" "main" {

    for_each = var.subnets

    vpc_id = aws_vpc.main.id

    availability_zone = each.value.az
    cidr_block = each.value.cidr

    tags = var.common_tags

}