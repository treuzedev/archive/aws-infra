#####
resource "aws_cloudfront_distribution" "cdn" {

  enabled = true
  aliases = ["cdn.treuze.dev"]
  tags    = var.common_tags

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = data.aws_acm_certificate.treuzedev.arn
    ssl_support_method = "sni-only"
  }

  origin {
  
    domain_name = aws_s3_bucket.cdn_bucket.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
    
    s3_origin_config {
        origin_access_identity = aws_cloudfront_origin_access_identity.oai_cdn.cloudfront_access_identity_path
    }
    
  }

  default_cache_behavior {

    allowed_methods        = ["HEAD", "GET"]
    cached_methods         = ["HEAD", "GET"]
    target_origin_id       = local.s3_origin_id
    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400


    forwarded_values {

      query_string = false

      cookies {
        forward = "none"
      }

    }

  }

}

resource "aws_cloudfront_origin_access_identity" "oai_cdn" {
  comment = "OAI for Just Bytes CDN Bucket"
}

resource "aws_route53_record" "cloudfront" {

  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "cdn.treuze.dev"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.cdn.domain_name
    zone_id                = aws_cloudfront_distribution.cdn.hosted_zone_id
    evaluate_target_health = true
    
  }
  
}


#####
data "aws_acm_certificate" "treuzedev" {
  provider = aws.useast1
  domain   = "treuze.dev"
  statuses = ["ISSUED"]
}

data "aws_route53_zone" "primary" {
  name         = "treuze.dev"
}


#####
locals {
  s3_origin_id = "justbytes-cdn-js"
}
