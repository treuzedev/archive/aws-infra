# AWS Chatbot

## Instructions

Set up policy: 
* https://docs.aws.amazon.com/chatbot/latest/adminguide/setting-up.html
* AWSChatbotTreuzeSlack
* arn:aws:iam::189942472391:policy/AWSChatbotTreuzeSlack

Set up role:
* From template
* arn:aws:iam::189942472391:role/service-role/AWSChatbotTreuzeSlack

Set up SNS Topic:
* https://docs.aws.amazon.com/sns/latest/dg/sns-getting-started.html
* AWSChatbotTreuzeSlack
* arn:aws:sns:eu-west-1:189942472391:AWSChatbotTreuzeSlack

Set up Chatbot:
* https://docs.aws.amazon.com/chatbot/latest/adminguide/getting-started.html
* slack-aws-chatbot
* arn:aws:chatbot::189942472391:chat-configuration/slack-channel/slack-aws-chatbot
